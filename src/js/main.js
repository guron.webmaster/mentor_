$(document).ready(function () {

    $(document).on('click', '.icon-menu', function (){
        $('.mobile-menu').toggle()
    })

    var defaultHeader = $('.page-header').html();


    function mobMenu(){
        if ( $(window).width() < 1260 ){

            $('.mobile-menu').append($('.page-header .menu-list'))
            $('.mobile-menu').append($('.page-header .nav-links'))

        }
        else {
            $('.page-header').html(defaultHeader)
        }
    }

    mobMenu()

    $(window).resize(function () {

        mobMenu()

    })


    $('.reviews-slider').slick({
        arrows : true
    });




    $('.form_select').each(function (i,el) {
        var selctor = $(el).find('.select_value');
        $(el).find('.select_options li').first().addClass('selected');
        $(el).find('.select_value').text($(el).find('.select_options li.selected').text());
        $(el).find('input').val($(el).find('.select_options li.selected').data('option'));

        selctor.click(function (){
            $('.form_select .select_value').removeClass('active')
            $(this).addClass('active')
            $('.form_select').find('.select_options').hide();
            $(el).find('.select_options').toggle();
        })

        var selectItem = $(el).find('.select_options li');
        selectItem.click(function (){
            selectItem.removeClass('selected');
            $(this).addClass('selected');
            $(el).find('.select_value').html($(this).html());
            $(el).find('input').val($(this).data('option'));
            $(el).find('.select_options').hide();
            $('.form_select .select_value').removeClass('active')
        })
    });

    $(document).mouseup(function (e)
    {
        var container = $(".form_select , .select_options"); // Give you class or ID

        if (!container.is(e.target) &&            // If the target of the click is not the desired div or section
            container.has(e.target).length === 0) // ... nor a descendant-child of the container
        {
            $('.select_options').hide();
            $('.form_select .select_value').removeClass('active')
        }
    });

    $('.price-wrap #price').val($('.price-wrap .block-el.active').data('value'))
    $('.price-wrap .block-el').click(function (){
        $('.price-wrap .block-el').removeClass('active')
        $(this).addClass('active')
        $('#price').val($(this).data('value'))
    })


    $(document).on('click', function () {
       if(event.target == $('.form-popup')[0]){
           $('body').removeClass('ov-hidden')
           $('.form-popup').fadeOut();
       }
    });
    $(document).on('click' , '.form-popup .close-form' , function () {
        $('body').removeClass('ov-hidden')
        $('.form-popup').fadeOut();
    });
    $(document).on('click' , '.toggleContacts' , function () {
        $('body').addClass('ov-hidden')
        $('.form-popup').css({top : $(window).scrollTop() + 'px'})
        $('.form-popup').fadeIn();
        $('.form-popup').css({'display' : 'flex'})
    });

});